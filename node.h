/*
 * node.h
 *
 *  Created on: Nov 24, 2011
 *      Author: rajat
 */

#ifndef NODE_H_
#define NODE_H_

#include <vector>
#include <string>
#include <map>
#include <sys/select.h>
#include "structures.h"

using namespace std;

typedef vector<string> VEC;

class Gnode{

private:
	const string conf_filename;
	map<string, string> config;

	string file_dir;

	int neighbor_port;
	int file_port;

	Address myaddr_np;
	Address myaddr_fp;

	int listener_np;
	int listener_fp;

	int max_number_of_peers;
	int current_number_of_peers;

	int max_TTL;
	bool seed;

	//To monitor connections
	fd_set master;
	fd_set read_fds;

	int highest_socket;

	vector<Address> host_cache;
	vector<Address> backup_host_cache;
	vector<Address> seed_nodes;



	//map<string, string> word_file_dict;
	map<string, string> word_file_dict;
	map<int, Connection> conn;

	//message_id = key and sockfd = val and val = -1 if locally generated packet
	map<long, int> packet_store;

	void populate_config();
	void populate_address_vectors();
	void populate_word_file_dict();

	vector<q_result_item> search_word_file_dict(string keyword);

public:

	//constructor
	Gnode(const string& conf_filename);

	//a function to show the configuration file content (debug purpose)
	void show()const;

	//Start up the listeners
	void start_listeners();

	//accessors
	int get_neighbor_port()const{return neighbor_port;}
	int get_file_port()const{return file_port;}
	int get_max_number_of_peers()const{return max_number_of_peers;}
	int get_current_number_of_peers()const{return current_number_of_peers;}
	int get_max_TTL()const{return max_TTL;}
	Address get_addr_np()const{return myaddr_np;}
	Address get_addr_fp()const{return myaddr_fp;}
	bool is_seed()const{return seed;}

	//connect using just tcp (not ping message) returns sockfd if unsuccessful return -1
	int connect_to_another_node(Address remote_node);
	void connect_to_neighbors();

	//Send a packet to peer
	int send_a_packet_to_peer(Address peer_addr, Packet* pkt);
	int send_a_packet_to_peer(int sockfd, Packet* pkt);

	void start_run();
	void read_from_active_sockets();

	void handle_ping_message(Packet* pkt, int sock);
	void handle_pong_message(Packet* pkt, int sock);
	void handle_query_message(Packet* pkt, int sock);
	void handle_query_hit_message(Packet* pkt, int sock);
	void handle_plain_messages(Packet* pkt, int sock);

	void handle_user_input(string msg);

	void forward_packet(Packet* pkt, int forbidden_sock);
};


void construct_packet(char* buf, int numbytes, Packet* pkt);

#endif /* NODE_H_ */
