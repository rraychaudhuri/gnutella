/*
 * networking.cpp
 *
 *  Created on: Dec 5, 2011
 *      Author: rajat
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>


#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "structures.h"
#include "node.h"
#include "string_utility.h"


using namespace std;


//Start the listeners
void Gnode::start_listeners(){

	struct sockaddr_in myaddr;
	//struct sockaddr_in tmp;
	//socklen_t p;

	FD_SET(fileno(stdin), &master);
	highest_socket = fileno(stdin) > highest_socket ? fileno(stdin) : highest_socket;

	//Create a socket for the neighbor port listener
	if ((listener_np = socket(PF_INET, SOCK_STREAM, 0)) == -1){
		cerr << "Could not create neighbor port listening socket. exiting..." << endl;
		exit(1);
	}

	//Create a socket for the file port listener
	if ((listener_fp = socket(PF_INET, SOCK_STREAM, 0)) == -1){
			cerr << "Could not create file port listening socket. exiting..." << endl;
			exit(1);
	}

	//Bind the socket for the neighbor port listener
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = INADDR_ANY;
	myaddr.sin_port = htons(neighbor_port);
	memset(&(myaddr.sin_zero), '\0', 8);

	if (bind(listener_np, (struct sockaddr *)&myaddr, sizeof(myaddr)) == -1){
		cerr << "Could not bind neighbor port to a listening socket. exiting..." << endl;
		exit(1);
	}

	if (listen(listener_np, 40) == -1){
		cerr << "Too many backlogs" << endl;
		exit(1);
	}

	cout << "Started listening at the neighbor port:" << neighbor_port << endl;

	//int chk = getsockname(listener_np, (struct sockaddr*) &tmp, &p);
	//cout << "\n\nMY IP:" << inet_ntoa(tmp.sin_addr) << "\t Retval:" << chk << endl;

	//myaddr_np.ip = string(inet_ntoa(tmp.sin_addr));
	//myaddr_np.port = neighbor_port;


	//Bind the socket for the file port listener
	myaddr.sin_family = AF_INET;
	myaddr.sin_addr.s_addr = INADDR_ANY;
	myaddr.sin_port = htons(file_port);
	memset(&(myaddr.sin_zero), '\0', 8);

	if (bind(listener_fp, (struct sockaddr *)&myaddr, sizeof(myaddr)) == -1){
		cerr << "Could not bind neighbor port to a listening socket. exiting..." << endl;
		exit(1);
	}

	cout << "Started listening at the file port:" << file_port << endl;

	if (listen(listener_fp, 40) == -1){
		cerr << "Too many backlogs" << endl;
		exit(1);
	}

	//getsockname(listener_fp, (struct sockaddr*) &tmp1, &p1);
	//cout << "\n\nMY IP:" << inet_ntoa(tmp1.sin_addr) << endl;

	//chk = getsockname(listener_fp, (struct sockaddr*) &tmp, &p);
	//cout << "\n\nMY FILE IP:" << inet_ntoa(tmp.sin_addr) << "\t Retval:" << chk << endl;

	//myaddr_fp.ip = myaddr_np.ip;
	//myaddr_fp.port = file_port;

	//Set the sockets up for listening
	FD_SET(listener_np, &master);
	FD_SET(listener_fp, &master);
	highest_socket = listener_np > highest_socket ? listener_np : highest_socket;
	highest_socket = listener_fp > highest_socket ? listener_fp : highest_socket;

}


//connect using just tcp (not ping message) returns sockfd if unsuccessful return -1
int Gnode::connect_to_another_node(Address remote_node){

	int sockfd = -1;
	Connection tmp(remote_node, sockfd);
	struct sockaddr_in server_addr;
	int res;

	Packet pkt(PLAIN, max_TTL);

	if ((sockfd = socket(PF_INET, SOCK_STREAM, 0)) == -1){
		cerr << "Could not create a socket to connect to a node" << endl;
		return -1;
	}

	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(remote_node.port);
	inet_aton(remote_node.ip.c_str(), (struct in_addr *)&server_addr.sin_addr.s_addr);
	memset(&(server_addr.sin_zero), '\0', 8);

	if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1){
		cerr << "Error connecting to " << remote_node.ip << " at port " << remote_node.port << endl;
		return -1;
	}

	//cout << "\n\nI am Here" << endl;

	Connection myconn(remote_node, sockfd);
	conn[sockfd] = myconn;

	FD_SET(sockfd, &master);
	highest_socket =  sockfd > highest_socket ? sockfd : highest_socket;

	//SEND A "CONNECT" MESSAGE
	pkt.set_message("CONNECT");
	res = send_a_packet_to_peer(sockfd, &pkt);
	//cout << "Send Returns:" << res << endl;

	//res  == -1 ? cerr << "Packet Sending Failed\n" : cout << "Packet Send\n";
	return res == -1 ? -1 : sockfd;
}


//Send a packet to peer
int Gnode::send_a_packet_to_peer(Address peer_addr, Packet* pkt){

	int sock = -1;

	//Find the corresponding socket
	for ( map<int, Connection>::const_iterator it=conn.begin() ; it != conn.end(); it++ ){
			    if(it->second.get_peerip() == peer_addr.ip
			    		&& it->second.get_peerport() == peer_addr.port
			    		&& it->second.is_active()){
			    	sock = it->first;
			    	break;
			    }
	}

	return sock == -1 ? -1 : send_a_packet_to_peer(sock, pkt);
}

//Send a packet to peer
int Gnode::send_a_packet_to_peer(int sockfd, Packet* pkt){

	//char* buf = (char*)malloc(sizeof(pkt));

	char* buf = new char[sizeof(*pkt)];
	memcpy(buf, (char*)pkt, sizeof(*pkt));

	//cout << "Packet Size:" << sizeof(*pkt) << endl;
	int res = send(sockfd, (void*)(buf), sizeof(*pkt), 0);

	if(res == -1){
		cerr << "Could not sent packet" << endl;
		return -1;
	}

	switch(pkt->pid){

	case PLAIN:
		cout << "Sent a PLAIN packet to host:" << conn[sockfd].get_peerip() << "\tMessage:"<<  pkt->msg << endl;
		break;
	case PING:
		cout << "Sent a PING packet to host:" << conn[sockfd].get_peerip() << endl;
		break;
	case PONG:
		cout << "Sent a PONG packet to host:" << conn[sockfd].get_peerip() << "\tMessage:"<<  pkt->msg << endl;
		break;
	case QUERY:
		cout << "Sent a QUERY packet to host:" << conn[sockfd].get_peerip() << "\tMessage:"<<  pkt->msg << endl;
		break;
	case QUERY_HIT:
		cout << "Sent a QUERY_HIT packet to host:" << conn[sockfd].get_peerip() << "\tMessage:"<<  pkt->msg << endl;
		break;
	case QUIT:
		cout << "Sent a QUIT packet to host:" << conn[sockfd].get_peerip() << endl;
		break;
	default:
		cout << "Sent an UNKNOWN packet to host:" << conn[sockfd].get_peerip() << "\tMessage:"<<  pkt->msg << endl;
		break;
	}

	return res;

}


void Gnode::start_run(){

	struct timeval timeout;
	timeout.tv_sec = 2;
	timeout.tv_usec = 0;



	while(true){

		read_fds = master;

		//Check for some problem
		if (select(highest_socket+1, &read_fds, NULL, NULL, &timeout) == -1){
			cerr << "Select Interrupted or there is a problem" << endl;
			exit(1);
		}

		read_from_active_sockets();
	}

}


void Gnode::read_from_active_sockets(){

	struct sockaddr_in remoteaddr;
	int newfd;
	socklen_t addrlen;
	Address tmp;


	if(listener_np != -1 && FD_ISSET(listener_np, &read_fds)){

		//**************************************
		// Handle New Connection Request
		//**************************************

		addrlen = sizeof(remoteaddr);
		if ((newfd = accept(listener_np, (struct sockaddr *)&remoteaddr, &addrlen)) == -1){

			cerr << "Problem in accepting new connection" << endl;
			//exit(1);

		}else{

			tmp.ip = inet_ntoa(remoteaddr.sin_addr);
			tmp.port = remoteaddr.sin_port;
			tmp.is_seed = -1;

			cout << "New connection from " <<  tmp.ip << " and port:" << tmp.port << endl;

			//If already have enough peers then close the connection
			if(max_number_of_peers <= current_number_of_peers){

				close(newfd);	//TAKE ACTION TO ADD TO BACKUP HOST CACHE

			}else{

				Connection myconn(tmp, newfd);
				conn[newfd] = myconn;

				FD_SET(newfd, &master);
				highest_socket = highest_socket > newfd ? highest_socket : newfd;

			}
		}

	}else if(listener_fp != -1 && FD_ISSET(listener_fp, &read_fds)){

		//**************************************
		// Handle new file transfer
		//**************************************



		cout << "\n\nReceived a connection request at the file transfer port\n\n";
		addrlen = sizeof(remoteaddr);

		if ((newfd = accept(listener_fp, (struct sockaddr *)&remoteaddr, &addrlen)) == -1){

			cerr << "Problem in accepting new connection" << endl;
			//exit(1);

		}else{

			tmp.ip = inet_ntoa(remoteaddr.sin_addr);
			tmp.port = remoteaddr.sin_port;
			tmp.is_seed = -1;

			cout << "New connection from " <<  tmp.ip << " and port:" << tmp.port << endl;

			Connection myconn(tmp, newfd);
			conn[newfd] = myconn;

			FD_SET(newfd, &master);
			highest_socket = highest_socket > newfd ? highest_socket : newfd;
		}




	}else if(FD_ISSET(fileno(stdin),&read_fds)){

		//**************************************
		// Handle new IO request
		//**************************************

		string str;
		getline(cin, str);
		handle_user_input(str);


	}else{

		//**************************************
		// Check all the existing connections
		//**************************************

		int nbytes;
		char buf[MAXBUFLEN];

		for(map<int, Connection>::iterator it=conn.begin(); it != conn.end(); it++){

			int i = it->first;

			if(!FD_ISSET(i, &read_fds))
				continue;


			nbytes = recv(i, buf, MAXBUFLEN, 0);

			if(nbytes <= 0){

				//Connection Closed
				if (nbytes == 0)
					cout << "Socket " << i << " hung up" << endl;

				FD_CLR(i, &master);

				if(conn.find(i) != conn.end() && conn[i].is_active()){
					current_number_of_peers--;
					conn[i].deactivate();
				}


			}else{

				//cout << "Received a packet at port:" << i  << "\tSize:" << nbytes << endl;
				Packet pkt;
				construct_packet(buf, nbytes, &pkt);

				//pkt.show();

				if(pkt.pid == PLAIN){

					cout << "Received a PLAIN packet from host:" << conn[i].get_peerip() << "\tMessage:"<<  pkt.msg << endl;
					handle_plain_messages(&pkt, i);

				}else if(pkt.pid == PING){

					if(!conn[i].is_active())
							continue;

					cout << "Received a PING packet from host:" << conn[i].get_peerip() << endl;
					handle_ping_message(&pkt, i);

				}else if(pkt.pid ==  PONG){

					if(!conn[i].is_active())
							continue;

					cout << "Received a PONG packet from host:" << conn[i].get_peerip() << "\tMessage:"<<  pkt.msg << endl;
					handle_pong_message(&pkt, i);

				}else if(pkt.pid ==  QUERY){

					if(!conn[i].is_active())
							continue;

					cout << "Received a QUERY packet from host:" << conn[i].get_peerip() << "\tMessage:"<<  pkt.msg << endl;
					handle_query_message(&pkt, i);

				}else if(pkt.pid ==  QUERY_HIT){

					if(!conn[i].is_active())
							continue;

					cout << "Received a QUERY_HIT packet from host:" << conn[i].get_peerip() << "\tMessage:"<<  pkt.msg << endl;
					handle_query_hit_message(&pkt, i);

				}else if(pkt.pid == QUIT){

					cout << "Received a QUIT packet from host:" << conn[i].get_peerip() << endl;
					conn[i].deactivate();
					current_number_of_peers--;

				}else{

					cout << "Message in packet:" << pkt.msg << endl;

				}
			}
		}
	}
}


void Gnode::connect_to_neighbors(){

	for(vector<Address>::iterator it=seed_nodes.begin(); it != seed_nodes.end(); it++)
		connect_to_another_node(*it);

}
