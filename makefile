CC = g++
CFLAGS = -g -Wall -pedantic

all: g04

g04: Gnutella.cpp strut.o msg.o node.o net.o  structures.h
	$(CC) $(CFLAGS) strut.o node.o net.o msg.o  Gnutella.cpp -o g04

node.o: structures.h node.h node.cpp strut.o
	$(CC) $(CFLAGS) -c node.cpp -o node.o

net.o: structures.h node.h networking.cpp
	$(CC) $(CFLAGS) -c networking.cpp -o net.o

msg.o: structures.h node.h msg_handler.cpp
	$(CC) $(CFLAGS) -c msg_handler.cpp -o msg.o

strut.o: structures.h string_utility.h  string_utility.cpp
	$(CC) $(CFLAGS) -c string_utility.cpp -o strut.o


clean: *.o
	rm -rf *.o
