/*
 * msg_handler.cpp
 *
 * Created on: Dec 6, 2011
 *      Author: rajat
 */

#include "node.h"
#include "structures.h"
#include "string_utility.h"

using namespace std;


void Gnode::handle_ping_message(Packet* pkt, int sock){

	int key = pkt->message_id + PING;

	if(packet_store.find(key) != packet_store.end()){

		//Discard since a PING msg with same id has been served before
		cout << "\n\nDISCARDING PING MSG\n\n";
		return;

	}

	packet_store[key] = sock;

	if(current_number_of_peers < max_number_of_peers && pkt->hops > 1){

		//Send PONG

		Packet p(PONG, max_TTL);
		p.message_id = pkt->message_id;
		p.set_message(address_to_string(myaddr_np));

		if(send_a_packet_to_peer(sock, &p) != -1){

			int key = pkt->message_id + PONG;
			packet_store[key] = -1;
		}

	}

	if(pkt->hops != pkt->TTL){

		pkt->hops += 1;
		forward_packet(pkt, sock);

	}

}


void Gnode::handle_pong_message(Packet* pkt, int sock){

	int key1 = pkt->message_id + PING;
	int key2 = pkt->message_id + PONG;

	if(packet_store.find(key1) == packet_store.end()){

		//Discard if I have not received PING for this PONG
		return;

	}else if(packet_store.find(key1) != packet_store.end() && packet_store[key1] == -1){

		//I generated the corresponding PING
		Address addr = string_to_address(pkt->msg);

		//Try to connect
		connect_to_another_node(addr);

	}else{

		//Forward the packet to the socket where the corresponding PING generated

		pkt->hops += 1;
		if(send_a_packet_to_peer(packet_store[key1], pkt) != -1)
			packet_store[key2] = sock;
	}
}


void Gnode::handle_query_message(Packet* pkt, int sock){

	int key = pkt->message_id + QUERY;

	if(packet_store.find(key) != packet_store.end()){
		//Discard since a PING msg with same id has been served before
		return;
	}

	//ADD PACKET TO THE LOCAL STORE
	packet_store[key] = sock;

	//Forward to all neighbors except the current socket
	forward_packet(pkt, sock);

	//SEARCH LOCAL STORE
	vector<q_result_item> res = search_word_file_dict(pkt->msg);

	if(res.size() == 0){

		cout << "No match found for message id:" << pkt->message_id << "\tKeyword:" << pkt->msg << endl;
		return;
	}

	Packet p(QUERY_HIT, max_TTL);
	p.message_id = pkt->message_id;

	string msg = address_to_string(myaddr_fp) + "*" + result_set_to_string(res);
	p.set_message(msg);

	send_a_packet_to_peer(sock, &p);

}


void Gnode::handle_query_hit_message(Packet* pkt, int sock){

	int key1 = pkt->message_id + QUERY;
	int key2 = pkt->message_id + QUERY_HIT;

	if(packet_store.find(key1) == packet_store.end()){

		//Discard if I have not received PING for this PONG
		return;

	}else if(packet_store.find(key1) != packet_store.end() && packet_store[key1] == -1){

		//I generated the QUERY message
		vector<string> x = my_split(pkt->msg, '*');

		if(x.size() < 2)
			return;

		Address addr = string_to_address(x[0]);
		vector<q_result_item> tmp = string_to_result_set(x[1]);

		cout << "Response for Message ID(QUERY):" << pkt->message_id << " Available at -> " << address_to_string(addr) << endl;

		for(vector<q_result_item>::iterator it = tmp.begin(); it != tmp.end(); it++)
			cout << "Index:" << it->index << "\tFilename:" << it->filename << "\tSize:" << it->size << endl;

		//connect_to_another_node(addr);

	}else{

		pkt->hops += 1;
		if(send_a_packet_to_peer(packet_store[key1], pkt) != -1)
			packet_store[key2] = sock;
	}

}


void Gnode::forward_packet(Packet* pkt, int forbidden_sock){

	//cout << "In forward sock" << endl ;

	for(map<int, Connection>::iterator it=conn.begin(); it != conn.end(); it++)
		if(it->first != forbidden_sock && it->second.is_active())
			send_a_packet_to_peer(it->first, pkt);

}


void Gnode::handle_user_input(string msg){

	vector<string> tmp = my_split(msg, ' ');
	string cmd = tmp[0];

	if(cmd.compare("quit") == 0)
		exit(0);

	if(cmd.compare("search") != 0){

		cout << "Invalid command from user:" << cmd;
		cout << "\nValid commands are: 'quit' or 'search'" << endl;
		return;

	}

	//cout << "Received a message from user -> command:" << cmd <<  "\targument:" << tmp[1] << endl;
	cout << "\nResult in the local Store:";

	vector<q_result_item> res = search_word_file_dict(tmp[1]);
	res.size() == 0 ? cout << "Null\n" : cout << endl;

	for(vector<q_result_item>::iterator it = res.begin(); it != res.end(); it++)
		cout << "Index:" << it->index << "\tFilename:" << it->filename << "\tSize:" << it->size << endl;

	Packet p(QUERY, max_TTL);
	p.set_message(tmp[1]);
	packet_store[(p.message_id+QUERY)] = -1;

	for(map<int, Connection>::iterator it=conn.begin(); it != conn.end(); it++){

		if(it->second.is_active())
			send_a_packet_to_peer(it->first, &p);

	}

}


void Gnode::handle_plain_messages(Packet* pkt, int sock){

	string tmp(pkt->msg);

	if(tmp.compare("CONNECT") == 0){

		if(current_number_of_peers < max_number_of_peers){

			Packet p(PLAIN, max_TTL);
			p.set_message("OK");

			int res = send_a_packet_to_peer(sock, &p);

			if(res != -1){

			 conn[sock].activate();
			 current_number_of_peers++;

			}

		}else{

			close(sock);
			conn[sock].deactivate();
			FD_CLR(sock, &master);

		}

	}else if(tmp.compare("OK") == 0){

		if(current_number_of_peers < max_number_of_peers){

			conn[sock].activate();
			current_number_of_peers++;


			if(current_number_of_peers < max_number_of_peers && !seed){

				Packet p(PING, max_TTL);
				packet_store[(p.message_id+PING)] = -1;

				send_a_packet_to_peer(sock, &p);

			}


		}else{

			close(sock);
			conn[sock].deactivate();
			FD_CLR(sock, &master);

		}

	}else{
		cout << "Socket " << sock << " said:" << pkt->msg << endl;
	}

}

