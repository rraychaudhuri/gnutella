//============================================================================
// Name        : Gnutella.cpp
// Author      : Rajat Raychaudhuri
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include "structures.h"
#include "node.h"
#include "string_utility.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main(int argc, char* argv[]) {

	srand(time(NULL));


	string filename = argc < 2 ? "g04.conf" : argv[1];


	Gnode a(filename);
	a.show();
	a.start_listeners();

	cout << endl << "**************************************************************" << endl;

	if(!a.is_seed())
		a.connect_to_neighbors();

	a.start_run();


	/*
	string tmp = "1:3000:sdfsf.txt;2:4000:er.txt;";
	vector<q_result_item> p = string_to_result_set(tmp);

	for(vector<q_result_item>::iterator it=p.begin(); it != p.end(); it++)
		cout << "Index:" << it->index << "\tSize:" << it->size << "\tName:" << it->filename << endl;

	tmp = result_set_to_string(p);
	cout << "\n" << tmp << endl;

	Address ad;
	ad.ip = "127.0.0.1";
	ad.port = 9000;

	tmp = address_to_string(ad);
	cout << tmp << endl;

	ad = string_to_address(tmp);
	cout << ad.ip << "\t" << ad.port << endl;

	for(int i=0; i<10; i++)
		cout << rand() << endl;

	*/


	return 0;
}
