/*
 * string_utility.cpp
 *
 *  Created on: Nov 24, 2011
 *      Author: rajat
 */

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <ctype.h>
#include <vector>
#include "string_utility.h"

using namespace std;



string address_to_string(Address addr){

	stringstream sstm;
	sstm << addr.ip << ":" << addr.port;

	return sstm.str();
}

Address string_to_address(string str){

	vector<string> tmp = my_split(str, ':');
	Address res;

	res.ip = tmp[0];
	res.port = atoi(tmp[1].c_str());
	res.is_seed = false;

	return res;
}

string result_set_to_string(vector<q_result_item> vec){

	stringstream sstm;

	for(vector<q_result_item>::iterator it=vec.begin(); it != vec.end() ; it++)
		sstm << it->index << ":" << it->size << ":" << it->filename << ";";

	return sstm.str();
}



vector<q_result_item> string_to_result_set(string str){

	vector<q_result_item> res;

	vector<string> tmp = my_split(str, ';');
	vector<string> tmp1;

	q_result_item t;

	for(vector<string>::iterator it=tmp.begin(); it != tmp.end(); it++){

		tmp1 = my_split(*it, ':');

		if(tmp1.size() != 3)
			continue;

		t.index = atoi(tmp1[0].c_str());
		t.size = atoi(tmp1[1].c_str());
		t.filename = tmp1[2];

		res.push_back(t);

	}
	return res;
}


//Custom function for lrtrim
string my_lrtrim(const string& str){

	size_t start=-1, end=string::npos, sz;

	for(size_t i = 0; i < str.length(); i++){
		if(!isspace(str[i])){
			start = i;
			break;
		}
	}


	for(size_t i = (str.length()-1); i >= 0; i--){
			if(!isspace(str[i])){
				end = i;
				break;
			}
		}

	if((signed)start == -1 || (signed)end == string::npos)
		return "";

	//cout << "start:" << start << "\tEnd:" << end << endl;
	sz = end - start + 1;
	return str.substr(start, sz);
}

//Split operation
vector<string> my_split(const string& mystr, char delm){

	vector<string> res;
	int start = -1, end = string::npos;

	string str = my_lrtrim(mystr);
	string t;

	for(size_t i=0; i < str.length(); i++){

		start = i;

		while(str[i] != delm && i < str.length())
			i++;

		end = i;
		t = str.substr(start, (end-start));
		//cout << start << "\t" << end << "\t" << t << endl;
		res.push_back(my_lrtrim(t));

	}

	return res;
}

