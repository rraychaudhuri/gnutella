/*
 * node.cpp
 *
 *  Created on: Nov 24, 2011
 *      Author: rajat
 */

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

#include <stdlib.h>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>
#include "structures.h"
#include "node.h"
#include "string_utility.h"
using namespace std;


//Constructor
Gnode::Gnode(const string& conf_filename):conf_filename(conf_filename){

	populate_config();
	populate_address_vectors();
	populate_word_file_dict();

	neighbor_port = atoi(config["neighborPort"].c_str());
	file_port = atoi(config["filePort"].c_str());

	max_number_of_peers = atoi(config["NumberOfPeers"].c_str());
	current_number_of_peers = 0;
	max_TTL = atoi(config["TTL"].c_str());

	int tmp = atoi(config["seedNode"].c_str());
	seed = tmp == 0 ? false : true;

	file_dir = config["localFilesDirectory"];

	listener_np = -1;
	listener_fp = -1;

	highest_socket = -1;
	FD_ZERO(&master);
	FD_ZERO(&read_fds);

	myaddr_np.ip = myaddr_fp.ip = config["IP"];
	myaddr_np.port = neighbor_port;
	myaddr_fp.port = file_port;
}


//Populates the map 'config'
void Gnode::populate_config(){

	string line;
	ifstream myfile (conf_filename.c_str());
	string key, value;

	//If file can not be opened, exit
	if (!myfile.is_open())
	{
		cerr << "Unable to open file";
		exit(0);
	}

	while ( myfile.good() )
	{
		getline (myfile,line);

		//if the line contains '#' ignore
		if(line.find_first_of('#') != string::npos)
			continue;

		//Split the line
		vector<string> tmp = my_split(line, '=');

		//If two strings not found, ignore
		if(tmp.size() != 2)
			continue;

		//Else populare the map 'config'
		config[tmp[0]] = tmp[1];

	}
	myfile.close();
}


//Display the contents of config
void Gnode::show()const{

	cout << "Neighbor Port:\t" << neighbor_port << endl;
	cout << "File Port:\t" << file_port << endl;
	cout << "Max TTL:\t" << max_TTL << endl;
	cout << "Seed ?\t" << (seed == true ? "Yes" : "No") << endl;
	cout << "Maximum peers:\t" << max_number_of_peers << endl;
	cout << "Current peers:\t" << current_number_of_peers << endl;
	cout << "Local File Directory:\t" << file_dir << endl;

	cout << "\nContent of Host Cache:" << endl;
	for(vector<Address>::const_iterator it = host_cache.begin(); it < host_cache.end(); it++)
		cout << it->ip << "\t" << it->port << endl;


	cout << "\nContent of Backup Host Cache:" << endl;
	for(vector<Address>::const_iterator it = backup_host_cache.begin(); it < backup_host_cache.end(); it++)
		cout << it->ip << "\t" << it->port << endl;


	cout << "\nSeed Nodes:" << endl;
	for(vector<Address>::const_iterator it = seed_nodes.begin(); it < seed_nodes.end(); it++)
		cout << it->ip << "\t" << it->port << endl;


	cout << "\nContent of local files:" << endl;
	for ( map<string, string>::const_iterator it=word_file_dict.begin() ; it != word_file_dict.end(); it++ )
		    cout << it->first << " => " << it->second << endl;

}


void Gnode::populate_word_file_dict(){

	ifstream myfile(config["localFiles"].c_str());
	string line;
	vector<string> words;
	vector<string> tmp;

	//If file can not be opened, exit
	if (!myfile.is_open())
	{
		cerr << "Unable to open word file dict" << endl;
		exit(0);
	}

	while ( myfile.good() )
	{
		getline(myfile,line);

		//if the line contains '#' ignore
		if(line.find_first_of('#') != string::npos)
			continue;

		//Split the line
		tmp = my_split(line, ':');

		//If two strings not found, ignore
		if(tmp.size() != 2)
			continue;

		//Else populate the map 'word_file_dict'
		word_file_dict[tmp[0]] = tmp[1];

	}
	myfile.close();

}


//populates all the address vectors
void Gnode::populate_address_vectors(){

	ifstream f1(config["hostCache"].c_str());
	ifstream f2(config["backupHostCache"].c_str());
	ifstream f3(config["seedNodes"].c_str());

	string line;
	vector<string> tmp;
	Address ad;

	if(!f1.is_open() || !f2.is_open() || !f3.is_open()){
		cerr << "could not open all the address files" << endl;
		exit(0);
	}

	while(f1.good()){

		getline(f1,line);

		//if the line contains '#' ignore
		if(line.find_first_of('#') != string::npos)
				continue;

		//Split the line
		tmp = my_split(line, ' ');

		//If two strings not found, ignore
		if(tmp.size() != 2)
			continue;

		//Else populare the map 'word_file_dict'
		ad.ip = tmp[0];
		ad.port = atoi(tmp[1].c_str());
		ad.is_seed = false;

		host_cache.push_back(ad);

	}

	f1.close();

	while(f2.good()){

			getline(f2,line);

			//if the line contains '#' ignore
			if(line.find_first_of('#') != string::npos)
					continue;

			//Split the line
			tmp = my_split(line, ' ');

			//If two strings not found, ignore
			if(tmp.size() != 2)
				continue;

			//Else populare the map 'word_file_dict'
			ad.ip = tmp[0];
			ad.port = atoi(tmp[1].c_str());
			ad.is_seed = false;

			backup_host_cache.push_back(ad);

		}


	f2.close();

	while(f3.good()){

			getline(f3,line);

			//if the line contains '#' ignore
			if(line.find_first_of('#') != string::npos)
					continue;

			//Split the line
			tmp = my_split(line, ' ');

			//If two strings not found, ignore
			if(tmp.size() != 2)
				continue;

			//Else populare the map 'word_file_dict'
			ad.ip = tmp[0];
			ad.port = atoi(tmp[1].c_str());
			ad.is_seed = true;

			seed_nodes.push_back(ad);

		}

	f3.close();
}



void construct_packet(char* buf, int numbytes, Packet* tmp){
	int count = 0;

	long *lng = new long;
	int* in = new int;

	memcpy(lng, buf, sizeof(long));
	count += sizeof(long);
	tmp->message_id = *lng;

	memcpy(in, buf+count, sizeof(int));
	count += sizeof(int);
	tmp->pid = *in;

	memcpy(in, buf+count, sizeof(int));
	count += sizeof(int);
	tmp->TTL = *in;

	memcpy(in, buf+count, sizeof(int));
	count += sizeof(int);
	tmp->hops = *in;

	memcpy(in, buf+count, sizeof(int));
	count += sizeof(int);
	tmp->payload_length = *in;

	memcpy(in, buf+count, sizeof(int));
	count += sizeof(int);
	tmp->number_of_hits = *in;

	//cout << "COUNT:" << count << "\tnbytes:" << numbytes << endl;
	memcpy(tmp->msg, buf+count, tmp->payload_length);
}


vector<q_result_item> Gnode::search_word_file_dict(string keyword){

	vector<string> res;
	vector<string> tmp;
	vector<q_result_item> resl;

	q_result_item t;

	for(map<string, string>::iterator it=word_file_dict.begin(); it != word_file_dict.end(); it++){
		tmp = my_split(it->second, '|');
		for(vector<string>::iterator ti=tmp.begin(); ti != tmp.end(); ti++){

			if(ti->compare(keyword) == 0){
				res.push_back(it->first);
				break;
			}
		}
	}

	int count = 0;
	struct stat st;
	string fname;

	for(vector<string>::iterator it= res.begin(); it != res.end(); it++){

		count++;
		fname = *it;

		fname =  config["localFilesDirectory"] + "/" + fname;
		stat(fname.c_str(), &st);

		t.filename = (*it);
		t.size = (int)st.st_size;
		t.index = count;

		resl.push_back(t);


	}

	return resl;
}

