/*
 * structures.h
 *
 *  Created on: Nov 24, 2011
 *      Author: rajat
 */

#ifndef STRUCTURES_H_
#define STRUCTURES_H_

#include<iostream>
#include<string>
#include<string.h>
#include<vector>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>


#define PING 1
#define PONG 2
#define QUERY 3
#define PLAIN 4
#define QUERY_HIT 5
#define QUIT 6

#define MSGSIZE 5000
#define MAXBUFLEN (MSGSIZE+100)

using namespace std;


//A structure to store IP-Port pair
typedef struct remote_address{

	string ip;
	int port;
	bool is_seed;

}Address;

typedef struct query_result_item{

	int index;
	int size;
	string filename;

}q_result_item;


class Connection{

private:

	Address peer_node;
	int sockfd;
	bool is_conn_active;

public:

	Connection(){
		sockfd = -1;
		peer_node.ip = "0.0.0.0";
		peer_node.port = -1;
		peer_node.is_seed = false;
		is_conn_active = false;
	}

	Connection(Address addr, int sock):sockfd(sock){
		peer_node.ip = addr.ip;
		peer_node.port = addr.port;
		peer_node.is_seed = addr.is_seed;
		is_conn_active = false;
	}

	Connection(const Connection& connt){
		peer_node.ip = connt.peer_node.ip;
		peer_node.port = connt.peer_node.port;
		peer_node.is_seed = connt.peer_node.is_seed;
		is_conn_active = connt.is_conn_active;
		sockfd = connt.sockfd;
	}

	//accessor
	int get_sockfd()const{return sockfd;}
	bool is_active()const{return is_conn_active;}
	string get_peerip()const{return peer_node.ip;}
	int get_peerport()const{return peer_node.port;}
	bool is_seed()const{return peer_node.is_seed;}

	void set_sockfd(int sock){sockfd = sock;}
	void deactivate(){is_conn_active = false;}
	void activate(){is_conn_active = true;}
};


class Packet{

public:

	long message_id;
	int pid;
	int TTL;
	int hops;
	int payload_length;
	int number_of_hits;
	char msg[MSGSIZE];

	Packet(){message_id = rand();}

	Packet(int mypid, int ttl)
	:pid(mypid),
	 TTL(ttl){

		//****************
		//SET MESSAGE ID
		//****************

		message_id = rand();
		hops = 1;

		number_of_hits = -1;
		payload_length = 0;

	}

	void set_message(string message){

		payload_length = message.size() + 1;
		strncpy(msg, message.c_str(), payload_length);
		//memcpy(msg, message.c_str(), payload_length);

		//cout << "\n\nMessage set to:" << msg << endl;

	}

	void show(){
		cout << "\nMSG ID:" << message_id ;
		cout << "\nPID:" << pid ;
		cout << "\nTTL:" << TTL ;
		cout << "\nHops:" << hops ;
		cout << "\npayload_length:" << payload_length ;
		cout << "\nHits:" << number_of_hits << "\nMSG:" << msg << endl;
	}
};

#endif /* STRUCTURES_H_ */
