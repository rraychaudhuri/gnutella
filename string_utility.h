/*
 * string_utility.h
 *
 *  Created on: Nov 24, 2011
 *      Author: rajat
 */

#ifndef STRING_UTILITY_H_
#define STRING_UTILITY_H_


#include <string>
#include <vector>
#include "structures.h"
using namespace std;

string my_lrtrim(const string& str);
vector<string> my_split(const string& str, char delm=' ');


string address_to_string(Address addr);
Address string_to_address(string str);

string result_set_to_string(vector<q_result_item> vec);
vector<q_result_item> string_to_result_set(string str);


#endif /* STRING_UTILITY_H_ */
